package main

import (
	"encoding/csv"
	"fmt"
	"io/ioutil"
	"log"
	"net"
	"net/http"
	"os"
	"time"
)

var curIP net.IP
var endpoint string
var csvFile = "ip-history.csv"

func main() {
	endpoint = os.Getenv("IP_RECORDER_ENDPOINT")
	if len(endpoint) <= 0 {
		endpoint = "https://ip.yrt.io/ip"
	}
	log.Printf("Endpoint: %s", endpoint)

	for true {
		time.Sleep(1 * time.Minute)
		newIP := getIP()
		log.Printf("DEBUG: Current IP: %s", newIP.String())
		if newIP == nil {
			handleError(fmt.Errorf("IP is nil"))
			continue
		}
		if newIP.Equal(curIP) {
			continue
		}

		log.Printf("INFO: IP Changed: Old %15s -> New %15s", curIP.String(), newIP.String())
		writeCSV(curIP, newIP)
		curIP = newIP
	}
}

func getIP() net.IP {
	resp, err := http.Get(endpoint)
	if err != nil {
		handleError(err)
		return nil
	}
	defer resp.Body.Close()
	if resp.StatusCode != http.StatusOK {
		handleError(fmt.Errorf("Status Code not 200. It's %d", resp.StatusCode))
		return nil
	}
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		handleError(err)
		return nil
	}
	ipString := string(body)

	ip := net.ParseIP(ipString)
	if ip == nil {
		handleError(fmt.Errorf("IP cannot parse. It's %s", ipString))
		return nil
	}
	return ip
}

func handleError(err error) {
	log.Printf("ERROR: %s", err.Error())
}

func writeCSV(old, new net.IP) {
	file, err := os.OpenFile(csvFile, os.O_APPEND|os.O_WRONLY, 0644)
	if err != nil {
		if !os.IsNotExist(err) {
			handleError(err)
			return
		}

		file, err = os.OpenFile(csvFile, os.O_CREATE|os.O_WRONLY, 0644)

		w := csv.NewWriter(file)
		record := []string{"Time", "Old IP", "New IP"}

		err = w.Write(record)
		if err != nil {
			handleError(err)
		}
		w.Flush()

		if err := w.Error(); err != nil {
			log.Fatal(err)
		}
	}

	t := time.Now()
	w := csv.NewWriter(file)
	record := []string{t.Format(time.RFC3339), old.String(), new.String()}

	err = w.Write(record)
	if err != nil {
		handleError(err)
	}
	w.Flush()

	if err := w.Error(); err != nil {
		log.Fatal(err)
	}
}
